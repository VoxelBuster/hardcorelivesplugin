package com.voxelbuster.hardcorelivesplugin.command;

import com.voxelbuster.hardcorelivesplugin.ConfigManager;
import com.voxelbuster.hardcorelivesplugin.HardcoreLivesPlugin;
import com.voxelbuster.hardcorelivesplugin.event.PlayerLifeCountChangedEvent;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

public class SubcommandGiveLives extends PluginSubcommand {
    protected SubcommandGiveLives(HardcoreLivesPlugin plugin) {
        super("giveLives", Optional.of("hardcorelives.lives.give"), plugin);

        this.description = "Show the number of lives for yourself or another player.";
        this.usage = "/hl giveLives " + ChatColor.YELLOW + "[player (default: yourself)]";
        this.aliases = Arrays.asList("givelives", "gl", "give", "gift");
    }

    @Override
    public boolean subcommandExecute(CommandSender sender, String alias, String[] args) {
        if (args.length != 2) {
            sendUsageMessage(sender);
            return false;
        }

        ConfigManager configManager = plugin.getConfigManager();

        if (!configManager.getConfig().isAllowBuyingLives()) {
            sender.sendMessage(ChatColor.DARK_RED + "This command is disabled.");
            sender.sendMessage(
                ChatColor.YELLOW + "If you are an admin and this is incorrect, check your config values.");
            return false;
        }

        if (!(sender instanceof Player issuingPlayer)) {
            sender.sendMessage(ChatColor.RED + "You must be a player to use this command.");
            return false;
        }

        Player target = Bukkit.getPlayer(args[0]);

        ConfigManager.PlayerData data = configManager.getPlayerData(issuingPlayer);
        ConfigManager.PlayerData dataTarget = configManager.getPlayerData(target);

        if (target == null) {
            sender.sendMessage(ChatColor.RED + "Player not found.");
            return true;
        }

        int numLives = Integer.parseInt(args[1]);

        if (numLives >= data.getLives() || numLives <= 0) {
            sender.sendMessage("You cannot give more lives than you have or less than 1 life.");
            return false;
        }

        data.setLives(data.getLives() - numLives);

        plugin.getServer().getPluginManager()
            .callEvent(new PlayerLifeCountChangedEvent(issuingPlayer, data.getLives()));
        dataTarget.setLives(dataTarget.getLives() + numLives);
        plugin.getServer().getPluginManager()
            .callEvent(new PlayerLifeCountChangedEvent(target, dataTarget.getLives()));

        issuingPlayer.sendMessage(
            ChatColor.GREEN + String.format("Sent %d lives to %s.", numLives, target.getName()));
        target.sendMessage(
            ChatColor.GREEN + String.format("%s sent you %d extra lives.", issuingPlayer.getName(), numLives));

        plugin.updateScoreboard();

        if (data.getLives() == 1) {
            issuingPlayer.sendMessage(
                ChatColor.YELLOW + String.format("You have %d more life remaining.", data.getLives()));
        } else {
            issuingPlayer.sendMessage(
                ChatColor.YELLOW + String.format("You have %d more lives remaining.", data.getLives()));
        }
        return true;

    }

    @Override
    public List<String> subCommandTabComplete(CommandSender sender, String alias, String[] args) throws IllegalArgumentException {
        if (args.length != 1) {
            return List.of();
        }

        ArrayList<String> optionNames = new ArrayList<>();
        plugin.getServer().getOnlinePlayers().forEach(p -> optionNames.add(p.getName()));

        if (sender instanceof Player) {
            optionNames.remove(sender.getName());
        }

        return optionNames.stream()
            .filter(s -> s.startsWith(args[0]))
            .toList();
    }
}
