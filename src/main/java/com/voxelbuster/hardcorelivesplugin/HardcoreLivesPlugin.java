package com.voxelbuster.hardcorelivesplugin;

import com.voxelbuster.hardcorelivesplugin.command.PluginBaseCommand;
import com.voxelbuster.hardcorelivesplugin.event.PluginEventHandler;
import com.voxelbuster.hardcorelivesplugin.placeholders.HardcoreLivesExpansion;
import lombok.Getter;
import net.milkbowl.vault.economy.Economy;
import net.milkbowl.vault.permission.Permission;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.PluginCommand;
import org.bukkit.entity.Player;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.Scoreboard;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Objects;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Logger;

import static java.util.logging.Level.SEVERE;

public final class HardcoreLivesPlugin extends JavaPlugin {

    @Getter
    private static final HardcoreLivesPlugin instance;

    @Getter
    private static Economy econ = null;

    @Getter
    private static Permission perms = null;

    static {
        instance = (HardcoreLivesPlugin) Bukkit.getPluginManager().getPlugin("Hardcorelivesplugin");
        assert instance != null;
    }

    final Logger log = this.getLogger();

    private final File dataFolder = this.getDataFolder();

    @Getter
    private final ExecutorService executor = Executors.newCachedThreadPool();
    @Getter
    private final ConfigManager configManager = new ConfigManager(this);
    @Getter
    private final File configFile = new File(Paths.get(dataFolder.toString(), "config.json").toString());
    @Getter
    private final File playersDir = new File(Paths.get(dataFolder.toPath().toString(), "players").toString());
    @Getter
    private final PluginEventHandler eventHandler = new PluginEventHandler(getConfigManager(), playersDir, this);
    @Getter
    private final SaveTaskScheduler saveTaskScheduler = new SaveTaskScheduler(this);
    @Getter
    private Objective scoreboardObjective;
    @Getter
    private boolean enabledBefore;
    @Getter
    private Scoreboard scoreboard;

    @Override
    public void onDisable() {
        // Plugin shutdown logic
        log.info("Saving config...");
        try {
            configManager.saveConfig(configFile);
        } catch (IOException e) {
            log.log(SEVERE, ChatColor.RED + "Failed to save config!", e);
        }
        log.info("Saving playerdata...");
        try {
            configManager.unloadAllPlayers(playersDir);
        } catch (IOException e) {
            log.log(SEVERE, ChatColor.RED + "Failed to save playerdata!", e);
        }
        log.info("Hardcore Lives disabled.");
    }

    @SuppressWarnings("DuplicatedCode")
    @Override
    public void onEnable() {
        log.info("Loading config...");

        handlePluginDirectory();

        if (this.getServer().isHardcore()) {
            log.warning("The server is in hardcore mode. Players will be banned on death " +
                "regardless of this plugin!\nPlease disable hardcore mode in server.properties to allow Lives to " +
                "work.");
        }

        if (setupEconomy()) {
            log.info("Vault Economy API Hooked.");
        } else {
            log.warning("Economy not hooked, features requiring economy will not work!");
        }

        if (setupPermissions()) {
            PermissionUtil.setPermissionManager(perms);
            log.info("Permission manager hooked.");
        } else {
            log.warning("Permission manager not detected.");
        }

        if (Bukkit.getPluginManager().getPlugin("PlaceholderAPI") != null) {
            new HardcoreLivesExpansion(this).register();
            log.info("PAPI hooked and HL expansion registered.");
        }

        PermissionUtil.registerPermissions();

        // Load online players (only does anything on reload)
        preloadPlayers();

        this.scoreboard = setupScoreboard();

        updateScoreboard();

        if (!enabledBefore) {
            this.getServer().getPluginManager().registerEvents(eventHandler, this);
        }

        PluginBaseCommand pluginCommandHandler = new PluginBaseCommand("hardcorelives", this);

        PluginCommand pluginMainCommand = this.getCommand("hardcorelives");

        if (pluginMainCommand == null) {
            log.severe("Cannot bind plugin command!");
            throw new IllegalStateException();
        }

        pluginMainCommand.setExecutor(pluginCommandHandler);
        pluginMainCommand.setTabCompleter(pluginCommandHandler);

        saveTaskScheduler.setSaveInterval(configManager.getConfig().getAutosaveUnit()
            .toMillis(configManager.getConfig().getAutosaveInterval()) / 50);
        saveTaskScheduler.rescheduleTask();

        this.enabledBefore = true;
    }

    private void preloadPlayers() {
        this.getServer().getOnlinePlayers().forEach(player -> {
            try {
                configManager.loadPlayerData(player, playersDir);
            } catch (IOException e) {
                log.log(SEVERE, "Exception loading player data!", e);
            }
        });
    }

    private void handlePluginDirectory() {
        try {
            if (!Files.isDirectory(dataFolder.toPath())) {
                Files.createDirectory(dataFolder.toPath());
            }

            configManager.loadConfig(configFile);

            // noinspection ResultOfMethodCallIgnored
            playersDir.mkdir();
        } catch (IOException e) {
            log.log(SEVERE, ChatColor.RED + "Could not create data directory! Will use default config values!", e);
        }
    }

    private boolean setupEconomy() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
        if (rsp == null) {
            return false;
        }
        econ = rsp.getProvider();
        return true;
    }

    private boolean setupPermissions() {
        if (getServer().getPluginManager().getPlugin("Vault") == null) {
            return false;
        }
        RegisteredServiceProvider<Permission> rsp = getServer().getServicesManager().getRegistration(Permission.class);
        if (rsp == null) {
            return false;
        }
        perms = rsp.getProvider();
        return true;
    }

    public void updateScoreboard() {
        Bukkit.getOnlinePlayers().forEach(this::updateScoreboard);
    }

    private Scoreboard setupScoreboard() {
        Scoreboard livesScoreboard = Objects.requireNonNullElseGet(getServer().getScoreboardManager(),
            () -> {
                throw new IllegalStateException("Plugin not loaded before world!");
            }).getNewScoreboard();

        this.scoreboardObjective = livesScoreboard.registerNewObjective("lives", "dummy", "Lives");
        this.scoreboardObjective.setDisplayName("Lives");
        this.scoreboardObjective.setDisplaySlot(configManager.getConfig().getScoreboardDisplaySlot());

        return livesScoreboard;
    }

    public void updateScoreboard(Player toUpdate) {
        Score playerLivesScore = this.scoreboardObjective.getScore(toUpdate.getName());
        playerLivesScore.setScore(configManager.getPlayerData(toUpdate).getLives());
    }

    @SuppressWarnings("DuplicatedCode")
    public void reload(boolean saveConfig) {
        log.info("Reloading HardcoreLives...");

        if (saveConfig) {
            log.info("Saving config...");
            try {
                configManager.saveConfig(configFile);
            } catch (IOException e) {
                log.log(SEVERE, ChatColor.RED + "Failed to save config!", e);
            }
        }

        log.info("Saving playerdata...");
        try {
            configManager.unloadAllPlayers(playersDir);
        } catch (IOException e) {
            log.log(SEVERE, ChatColor.RED + "Failed to save playerdata!", e);
        }

        log.info("Hardcore Lives disabled.");

        log.info("Loading config...");

        try {
            if (!Files.isDirectory(dataFolder.toPath())) {
                Files.createDirectory(dataFolder.toPath());
            }

            File configFile = new File(Paths.get(dataFolder.toString(), "config.json").toString());
            configManager.loadConfig(configFile);
            // noinspection ResultOfMethodCallIgnored
            playersDir.mkdir();
        } catch (IOException e) {
            log.log(SEVERE, ChatColor.RED + "Could not create data directory! Will use default config values!", e);
        }

        if (this.getServer().isHardcore()) {
            log.warning("The server is in hardcore mode. Players will be banned on death " +
                "regardless of this plugin!\nPlease disable hardcore mode in server.properties to allow Lives to " +
                "work.");
        }

        if (setupEconomy()) {
            log.info("Vault API Hooked.");
        } else {
            log.warning("Vault not found, features that require economy will not work.");
        }

        this.scoreboard = setupScoreboard();

        this.getServer().getOnlinePlayers().forEach(p -> {
            try {
                configManager.loadPlayerData(p, playersDir);
            } catch (IOException e) {
                log.log(SEVERE, String.format(ChatColor.RED + "Failed to load player %s (UUID %s)!", p.getName(),
                    p.getUniqueId()), e);
            }
        });

        updateScoreboard();
    }
}
