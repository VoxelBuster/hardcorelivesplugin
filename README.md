# HardcoreLivesPlugin

A plugin that allows you to give players limited lives on a survival server.
Simply drop the jar into your plugins folder and you're ready to go.

**Note:** The server must not actually be in hardcore mode for the plugin to work.
Hardcore will ban players on death regardless of plugins. Please ensure `hardcore=false`
in your server.properties.

**Note (2):** This plugin was built on top of Spigot and tested with PaperSpigot. Unofficial forks of Spigot are NOT
supported and you will be asked to reproduce your issue on one of these official server distributions 
if you are seeking support.

------

#### Commands:

`/hl` - Show Help

`/hl lives` - Check the number of lives you have

`/hl scoreboard` - Toggles the lives scoreboard for yourself.

`/hl lives buy <number>` - Purchase lives at the buy price set by the config. 
This will not allow you to leave yourself with less than 1 life. ***Requires Vault***

`/hl lives sell <number>` - Sell lives at the sell price set by the config. 
This will not allow you to leave yourself with less than 1 life. ***Requires Vault***

`/hl giveLives <player> <number>` - Transfer lives to another player. 
This will not allow you to leave yourself with less than 1 life.

`/hl lives <player>` - Check the number of lives another player has

`/hl resetAll` - Reset all player data and life counts

`/hl reload` - Reload the plugin from config

`/hl reset <player>` - Reset the data for the given player

`/hl config <key> <value>` - change the config file for the given key

`/hl setLives <player> <lives>` - change the number of lives a player has remaining

`/hl addLives <player> <lives>` - add to a player's life count. 

*Note that the above 2 commands will not exceed the maximum life count if one is set.*

`/hl setMaxLives <player> <max>` - change the maximum number of lives a player can have (default: 3; set to 0 for no max)

`/hl scoreboardSlot <SIDEBAR/PLAYER_LIST/BELOW_NAME>` - change the scoreboard display slot for the plugin. 
**NOTE:** this is a server wide change.

`/hl save` - Saves all player data to disk. This also happens automatically for a single player when a player leaves the 
server or the server is shut down gracefully.

`/hl autosave <number> <TimeUnit>` - Sets the auto save period for the plugin. The Time Unit argument can be any of the 
following: `MICROSECONDS`, `MILLISECONDS`, `SECONDS`, `MINUTES`, `HOURS`, `DAYS`. It is not recommended you set this to
less than 10 seconds. **Default:** *5 minutes*

------

#### Config:

`startingLives` - *Integer* : Number of lives a player starts with when they first join the server.
Default: `3`

`allowTotemOfUndying` - *Boolean* : Setting to `false` prevents totem of undying from working:
players will always die upon taking lethal damage. Default: `true`

`spectateWhenNoMoreLives` - *Boolean* : Setting to `false` will cause players to be banned once they run out of lives.
Default: `true`

`autosaveInterval` - *Integer* : Sets the interval that player data is autosaved. Works in conjunction with 
`autosaveUnit`. It is not recommended you set these values lower than 10 seconds. This cannot be set by `/hl config`.

`autosaveUnit` - *String*: Any of the following: `MICROSECONDS`, `MILLISECONDS`, `SECONDS`, `MINUTES`, `HOURS`, `DAYS`.
This cannot be set by `/hl config`.

`allowBuyingLives` - *Boolean* : Setting to true allows players with proper permissions to use `/hl lives buy`

`allowSellingLives` - *Boolean* : Setting to true allows players with proper permissions to use `/hl lives sell`

`allowGivingLives` - *Boolean* : Setting to true allows players with proper permissions to use `/hl lives give`

`lifeBuyPrice` - *Float* : Sets the buy price of a single life for all players.

`lifeSellPrice` - *Float* : Sets the sell price of a single life for all players.

`scoreboardDisplaySlot` - *String* : Sets the display slot of the lives scoreboard. Valid values are `SIDEBAR`, 
`BELOW_NAME`, or `PLAYER_LIST`. 

`loseLifeOnPvpOnly` - *Boolean* : Setting to true causes players to only lose lives when they die in PVP combat.
Default: `false`

------

#### Permissions
- `hardcorelives.lives.others`
- `hardcorelives.lives.buy`
- `hardcorelives.lives.sell`
- `hardcorelives.lives.give`
- `hardcorelives.reset`
- `hardcorelives.resetAll`
- `hardcorelives.scoreboard`
- `hardcorelives.scoreboard.manage`
- `hardcorelives.reload`
- `hardcorelives.config`
- `hardcorelives.setLives`
- `hardcorelives.bypass`
- `hardcorelives.save`
- `hardcorelives.autosave`

------

#### Placeholders:
- `%hardcorelivesplugin_lives%`
- `%hardcorelivesplugin_max_lives%`

#### Other info:

 - Opped players will not be forced into spectator mode even if they have zero lives. This
 is due to the fact that checking lives checks the `hardcorelives.bypass` permission, which ops always have.
 - Resetting or manually overriding a player's lives does not change their gamemode back. Be sure to `/gamemode survival`
 
